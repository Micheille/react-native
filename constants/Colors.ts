export const accent = '#373AF5';
export const secondary = '#8083A4';
export const baseStrong = '#2c2d76';
export const baseWeak = '#ffffff';
export const critic = '#e5596d';
