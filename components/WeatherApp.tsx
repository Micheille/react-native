import React from 'react';
import { StyleSheet, View, ScrollView, ImageBackground } from 'react-native';

import UbuntuText from './UbuntuText';
import WeatherFormPast from './WeatherFormPast';
import { accent, baseWeak } from '../constants/Colors';

export default function WeatherApp() {
  return (
    <ScrollView style={styles.weatherApp}>
      <ImageBackground
        source={require('../assets/background/Academy-Weather-forecast-Bg-buttom.png')}
        resizeMode='cover'
        style={{
          flex: 1,
        }}
      >
        <ImageBackground
          source={require('../assets/background/Academy-Weather-forecast-Bg-up.png')}
          resizeMode='cover'
          style={{
            flex: 1,
          }}
        >
          <View style={styles.appContainer}>
            <View style={styles.header}>
              <View>
                <UbuntuText
                  bold
                  style={[
                    styles.titleWord,
                    styles.titleWordWeather,
                    { fontFamily: 'Ubuntu-Bold' },
                  ]}
                >
                  Weather
                </UbuntuText>

                <UbuntuText
                  bold
                  style={[styles.titleWord, styles.titleWordForecast]}
                >
                  forecast
                </UbuntuText>
              </View>
            </View>

            <View style={styles.main}>
              <WeatherFormPast />
            </View>

            <View style={styles.footer}>
              <UbuntuText style={styles.meta}>
                С любовью от Mercury Development
              </UbuntuText>
            </View>
          </View>
        </ImageBackground>
      </ImageBackground>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  weatherApp: {
    flexDirection: 'column',
    width: '100%',
    height: 'auto',
    backgroundColor: accent,
  },
  appContainer: {
    alignItems: 'center',
    minHeight: '100%',
  },
  header: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    flexGrow: 0,
    marginVertical: 0,
    marginHorizontal: 'auto',
    width: 243,
    height: 'auto',
    paddingVertical: 32,
  },
  titleWord: {
    color: baseWeak,
    fontSize: 32,
    lineHeight: 36,
  },
  titleWordWeather: {
    textAlign: 'left',
  },
  titleWordForecast: {
    textAlign: 'right',
  },
  main: {
    width: '100%',
  },
  footer: {
    flexGrow: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    paddingVertical: 16,
  },
  meta: {
    fontSize: 14,
    lineHeight: 18,
    color: baseWeak,
    opacity: 0.6,
    textTransform: 'uppercase',
  },
});
