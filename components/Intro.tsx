import React from 'react';
import { StyleSheet, View, Image } from 'react-native';

import UbuntuText from '../components/UbuntuText';
import { secondary } from '../constants/Colors';

export default function Intro() {
  return (
    <View style={styles.intro}>
      <View>
        <Image
          style={styles.image}
          source={require('../assets/placeholder/Academy-Weather-bg160.png')}
        />
      </View>

      <UbuntuText bold style={styles.text}>
        Fill in all the fields and the weather will be displayed
      </UbuntuText>
    </View>
  );
}

const styles = StyleSheet.create({
  intro: {
    alignItems: 'center',
  },
  image: {
    height: 160,
    width: 160,
  },
  text: {
    textAlign: 'center',
    color: secondary,
  },
});
