import React from 'react';
import { StyleSheet, View, Image } from 'react-native';

import UbuntuText from '../components/UbuntuText';
import { accent, baseWeak } from '../constants/Colors';

const monthsAbbreviated: { [key: number]: string } = {
  1: 'jan',
  2: 'feb',
  3: 'mar',
  4: 'apr',
  5: 'may',
  6: 'jun',
  7: 'jul',
  8: 'aug',
  9: 'sep',
  10: 'oct',
  11: 'nov',
  12: 'dec',
};

export default function CardPast({
  date,
  icon,
  temperatureKelvin,
}: {
  date: Date;
  icon: string;
  temperatureKelvin: number;
}) {
  const month = monthsAbbreviated[date.getMonth()];
  const day = date.getDate();
  const year = date.getFullYear();
  const temperatureCelsius = temperatureKelvin - 273;

  return (
    <View style={styles.cardPast}>
      <UbuntuText
        bold
        style={[styles.text, styles.date]}
      >{`${day} ${month} ${year}`}</UbuntuText>
      <Image
        style={styles.icon}
        source={{ uri: `https://openweathermap.org/img/wn/${icon}@2x.png` }}
      />
      <UbuntuText
        bold
        style={[styles.text, styles.temperature]}
      >{`+ ${Math.round(temperatureCelsius)}°`}</UbuntuText>
    </View>
  );
}

const styles = StyleSheet.create({
  cardPast: {
    width: 252,
    height: 258,
    backgroundColor: accent,
    alignSelf: 'center',
    padding: 20,
    borderRadius: 8,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  text: {
    color: baseWeak,
  },
  date: {
    fontSize: 16,
  },
  icon: {
    width: 160,
    height: 160,
    alignSelf: 'center',
  },
  temperature: {
    textAlign: 'right',
    fontSize: 32,
  },
});
