import React, { useState } from 'react';
import { Text } from 'react-native';
import * as Font from 'expo-font';
import AppLoading from 'expo-app-loading';

const getFonts = () =>
  Font.loadAsync({
    'Ubuntu-Bold': require('../assets/fonts/Ubuntu/Ubuntu-Bold.ttf'),
    'Ubuntu-Regular': require('../assets/fonts/Ubuntu/Ubuntu-Regular.ttf'),
  });

type UbuntuTextProps = {
  bold?: boolean;
};

export default function UbuntuText(props: UbuntuTextProps & Text['props']) {
  const [fontsLoaded, setFontsLoaded] = useState(false);

  const fontStyle = props.bold
    ? { fontFamily: 'Ubuntu-Bold' }
    : { fontFamily: 'Ubuntu-Regular' };

  if (fontsLoaded) return <Text {...props} style={[props.style, fontStyle]} />;
  else {
    return (
      <AppLoading
        startAsync={getFonts}
        onFinish={() => setFontsLoaded(true)}
        onError={() => {}}
      />
    );
  }
}
