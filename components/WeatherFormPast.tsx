import React, { useEffect, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import { DatePicker } from 'react-native-woodpicker';

import UbuntuText from '../components/UbuntuText';
import Intro from './Intro';
import CardPast from './CardPast';
import { baseWeak, baseStrong, critic } from '../constants/Colors';

type City = {
  lat: number;
  lon: number;
};

const cities: { [key: string]: City } = {
  Samara: {
    lat: 53.195873,
    lon: 50.100193,
  },
  Tolyatti: {
    lat: 53.507836,
    lon: 49.420393,
  },
  Saratov: {
    lat: 51.533557,
    lon: 46.034257,
  },
  Kazan: {
    lat: 55.796127,
    lon: 49.106405,
  },
  Krasnodar: {
    lat: 55.796127,
    lon: 38.975313,
  },
};

export default function WeatherFormPast() {
  const [latitude, setLatitude] = useState<number | null>(null);
  const [longitude, setLongitude] = useState<number | null>(null);
  const [city, setCity] = useState<string>();
  const [date, setDate] = useState<Date>();
  const [time, setTime] = useState<number>(0);
  const [isLoaded, setLoaded] = useState<boolean>(false);
  const [error, setError] = useState<string>('');
  const [icon, setIcon] = useState<string>('');
  const [temperatureKelvin, setTemperatureKelvin] = useState<number>(0);

  const dateFiveDaysAgo = new Date();
  dateFiveDaysAgo.setDate(dateFiveDaysAgo.getDate() - 5);

  const getHistoricalWeather = async (
    lat: number,
    lon: number,
    time: number
  ) => {
    let response = new Response();
    try {
      response = await fetch(
        `https://api.openweathermap.org/data/2.5/onecall/timemachine?lat=${lat}&lon=${lon}&dt=${time}&appid=f4ea689bc636fcf4c6e9a08ea5f9f7f8`
      );
    } catch (error) {
      setError('we miss your internet connection');
      setLoaded(false);
    } finally {
      const historicalWeatherData = await response.json();

      if (!response.ok) {
        setError(historicalWeatherData.message);
        setLoaded(false);
      } else {
        setIcon(historicalWeatherData.current.weather[0].icon);
        setTemperatureKelvin(historicalWeatherData.current.temp);
        setLoaded(true);
      }
    }
  };

  useEffect(() => {
    latitude &&
      longitude &&
      time &&
      getHistoricalWeather(latitude, longitude, time);
  }, [latitude, longitude, time]);

  const handleCityChange = (cityValue: string) => {
    setCity(cityValue);
    setLatitude(cities[cityValue].lat);
    setLongitude(cities[cityValue].lon);
  };

  const handleDateChange = (date?: Date) => {
    setDate(date);
    if (date) {
      const timeMilliseconds = date.getTime();
      const timeSeconds = Math.floor(timeMilliseconds / 1000);
      setTime(timeSeconds);
    }
  };

  const handleText = (): string =>
    date
      ? `${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`
      : 'Select date';

  return (
    <View style={styles.weatherFormPast}>
      <UbuntuText bold style={styles.title}>
        Forecast for a Date in the Past
      </UbuntuText>

      <View style={styles.pickerBorder}>
        <Picker
          selectedValue={city}
          onValueChange={handleCityChange}
          style={styles.pickerCity}
        >
          <Picker.Item label='Select city' enabled={false} />
          <Picker.Item label='Samara' value='Samara' />
          <Picker.Item label='Tolyatti' value='Tolyatti' />
          <Picker.Item label='Saratov' value='Saratov' />
          <Picker.Item label='Kazan' value='Kazan' />
          <Picker.Item label='Krasnodar' value='Krasnodar' />
        </Picker>
      </View>

      <View style={[styles.pickerBorder, styles.pickerBorderDate]}>
        <DatePicker
          style={styles.pickerDate}
          text={handleText()}
          onDateChange={handleDateChange}
          androidDisplay='calendar'
          isNullable
          locale='ru'
          minimumDate={dateFiveDaysAgo}
          maximumDate={new Date()}
          textInputStyle={{ color: baseStrong }}
        />
      </View>

      <View>
        {isLoaded && date ? (
          <CardPast
            date={date}
            icon={icon}
            temperatureKelvin={temperatureKelvin}
          />
        ) : (
          <Intro />
        )}
        <UbuntuText bold style={styles.error}>
          {!isLoaded && error && `Sorry, ${error}!`}
        </UbuntuText>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  weatherFormPast: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: baseWeak,
    borderRadius: 8,
    paddingVertical: 32,
    paddingHorizontal: 24,
    marginHorizontal: 10,
  },
  title: {
    marginBottom: 32,
    fontSize: 32,
    color: baseStrong,
  },
  pickerBorder: {
    borderWidth: 2,
    borderColor: 'rgba(128, 131, 164, 0.2)',
    borderRadius: 8,
    backgroundColor: 'rgba(128, 131, 164, 0.06)',
  },
  pickerBorderDate: {
    marginVertical: 24,
  },
  pickerCity: {
    height: 48,
    fontSize: 16,
    color: baseStrong,
  },
  pickerDate: {
    height: 48,
    fontSize: 16,
    color: baseStrong,
    paddingHorizontal: 8,
    paddingRight: 14,
  },
  error: {
    color: critic,
  },
});
